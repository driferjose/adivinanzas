import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController  } from 'ionic-angular';
import { Servidor } from '../../servicios/servidor.service';
import { HomePage } from '../home/home'

@IonicPage()
@Component({
  selector: 'page-transicion',
  templateUrl: 'transicion.html',
  providers : [Servidor]
})

export class TransicionPage {

  public oro_actual = 0;
  private oro_ganado = 50;
  private interval ;
  public respuesta = "";
  private siguiente_nivel : 0;
  public permitir_continuar = false;
  public fin_juego = false
  public continuar_btn = false
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewCtrl: ViewController,
              private servidor : Servidor  ) {
  }

  ionViewDidLoad(){
    this.recibir_parametros();
    this.servidor.ga.trackView('Pagina trancision');
  }
  
  ionViewDidLeave(){
    // if(this.siguiente_nivel % 3 == 0 ){
    //   this.servidor.admob.showInterstitial();
    // }
  }
  
  ionViewCanLeave(): boolean {
    if(this.permitir_continuar) {
      if(!this.continuar_btn) {
        this.enviar_parametros_a_juego()
      }
      return true
    }else {
      return false
    }
  }

  recibir_parametros(){
    let params = this.navParams.get("datos");
    this.oro_actual = params.oro_actual;
    this.oro_ganado = params.oro_ganado;
    this.respuesta = params.respuesta;
    this.siguiente_nivel = params.siguiente_nivel;
    if( this.servidor.preguntas.preguntas.length < this.siguiente_nivel ) {
      this.fin_juego = true
    }
    this.animar_oro();
  }

  animar_oro(){
    this.iniciar_interval();
  }

  continuar(){
    if( this.permitir_continuar == true ){
      this.continuar_btn = true
      this.enviar_parametros_a_juego()
    }
  }
  
  public enviar_parametros_a_juego () {
    if(!this.fin_juego) {
      let data = {
        data : {
          oro : this.oro_actual,
          nivel : this.siguiente_nivel
        }
      };
      this.viewCtrl.dismiss(data);
      this.servidor.admob.showInterstitial()
    }else{
      this.navCtrl.setRoot(HomePage)
    }
  }

  iniciar_interval(){
    let este = this
    let total = this.oro_actual + this.oro_ganado
    let pasos = this.oro_ganado * 0.1

    this.interval = setInterval(()=>{
      if(este.oro_actual < total){
        este.oro_actual += pasos
      }else{
        this.permitir_continuar = true
        clearInterval(this.interval)
      }
    }, 50)
  }
}
