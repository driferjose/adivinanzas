import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Servidor } from '../../servicios/servidor.service';

@IonicPage()
@Component({
  selector: 'page-niveles',
  templateUrl: 'niveles.html',
  providers : [Servidor]
})
export class NivelesPage {
  public niveles = [];
  public nivel_escogido = 1;
  public nivel_max_liberado = 1;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private servidor : Servidor,
              public events: Events ) {
    this.recibir_parametros();
    this.llenar_niveles();
  }

  ionViewDidLoad() {
    this.servidor.ga.trackView('Pagina de niveles');
  }

  ionViewDidLeave(){
    let packete = {
      nivel_escogido : this.nivel_escogido,
    }
    this.events.publish('nuevo_nivel',packete);
    this.servidor.iniciar_interstitial();
  }

  recibir_parametros(){
    let parametros = this.navParams.get("datos_user");
    this.nivel_escogido = parametros.nivel_escogido;
    this.nivel_max_liberado = parametros.nivel_max;
  }

  seleccionar(e){
    if(this.niveles[e].estado == "desbloqueado"){
      for (var i = 0; i < this.niveles.length; i++) {
        this.niveles[i].seleccionado = false;
      }
      this.niveles[e].seleccionado = true;
      this.nivel_escogido = e+1;
      this.servidor.ga.trackEvent("Niveles","escoger nivel",this.nivel_escogido+"")
    }
  }

  llenar_niveles(){
    for (var i = 1; i <= this.servidor.preguntas.preguntas.length; i++) {
      let modelo = {nivel : i, estado : "bloqueado", seleccionado : false };
      if(i <= this.nivel_max_liberado){
        modelo.estado = "desbloqueado";
      }
      if(i == this.nivel_escogido){
        modelo.seleccionado = true;
      }
      this.niveles.push(modelo);
    }
  }
}
