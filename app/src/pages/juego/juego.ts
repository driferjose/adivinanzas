import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, Events   } from 'ionic-angular';
import { Servidor } from '../../servicios/servidor.service';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-juego',
  templateUrl: 'juego.html',
  providers : [Servidor]
})

export class JuegoPage {

  public letras_respuesta =[];
  public teclado_resp = [];
  private banco_letras = ["A","B","C","D","E","F",
                          "G","H","I","J","K","L",
                          "M","N","Ñ","O","P","Q",
                          "R","S","T","U","V","W",
                          "X","Y","Z"];
  private indice_llenar = null;
  public pista_txt = false;
  private respuesta ="";
  private indices_a_llenar = [];
  public oro = 0;
  public nivel = 0;
  public pregunta = "";
  public pista_texto = "";
  public nivel_max = 1 ;
  private costo_pista_txt = 50;
  private costo_pista_casilla = 100;
  private opciones_alert = {
    title: 'Escoge un tipo de pista',
    inputs: [
      {
        label: 'Rellenar casilla (costo: 100 de oro) ',
        type:'radio',
        value:'casilla'
      },
      {
        label: 'Texto (costo: 50 de oro)',
        type:'radio',
        value:'texto'
      }
    ],
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: data => {

        }
      },
      {
        text: 'Ok',
        handler: data => {
          this.mostrar_pista(data);
          this.servidor.ga.trackEvent("Juego","mostrar pista",data)
        }
      }
    ]
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              private servidor : Servidor,
              private socialSharing: SocialSharing,
              public events: Events ) {
    this.recibir_parametros();
    this.iniciar_juego();
  }

  ionViewDidLoad() {
    this.servidor.ga.trackView('Pagina de juego');
  }
  ionViewDidLeave(){
    this.guardar_avanze();
    this.servidor.iniciar_interstitial();
  }

  compartir_adivinanza(){
    let param = {
      message: this.pregunta,
      subject: "A que no puedes adivinar esto",
      files: "",
      url: "string",
      chooserTitle: "Comparte esta adivinanza"
    }
    this.socialSharing.shareWithOptions(param)
    .then((data)=>{
      
    })
  }

  iniciar_juego(){
    this.pregunta = this.servidor.preguntas.preguntas[this.nivel-1].pregunta;
    this.respuesta = this.servidor.preguntas.preguntas[this.nivel-1].respuesta.toUpperCase();
    this.pista_texto = this.servidor.preguntas.preguntas[this.nivel-1].pista;
    this.crear_arreglo_respuesta();
    this.crear_teclado();
  }

  guardar_avanze(){
    let packete = {
      nivel_max : this.nivel,
      nivel_escogido : this.nivel,
      oro_actual : this.oro
    }
    this.servidor.guardar_data_user(packete);
    this.events.publish('juego_fin',packete);
  }

  private recibir_parametros(){
    let parametros = this.navParams.get("datos_user");
    this.oro = parametros.oro_actual;
    this.nivel = parametros.nivel_escogido;
    this.nivel_max = parametros.nivel_max;
    this.pista_txt = false;
  }

  private restar_oro(cantidad){
    if(this.oro >= cantidad){
      this.oro -= cantidad;
    }
  }

  mostrar_transicion(){
    let parametros = {
      datos : {
        oro_actual : this.oro,
        oro_ganado : 20,
        respuesta : this.respuesta,
        siguiente_nivel : this.nivel + 1
      }
    }
    if(this.nivel_max > this.nivel){
      parametros.datos.oro_ganado = 10;
    }
    let profileModal = this.modalCtrl.create( "TransicionPage", parametros );
    profileModal.onDidDismiss( data => {
      if(data.data.nivel+1 % 3 == 0) {
        this.servidor.iniciar_interstitial();
      }
      if(data) {
        this.continuar_juego(data);
        this.servidor.ga.trackEvent("Juego","oro ganado",this.oro+"")
        this.servidor.ga.trackEvent("Juego","Siguiente nivel",this.nivel+"")
      }
    });
    profileModal.present();
  }

  private continuar_juego(params){
    this.oro = params.data.oro;
    this.nivel = params.data.nivel;
    this.pista_txt = false;
    if(this.opciones_alert.inputs.length == 1) {
      this.opciones_alert.inputs.push({
        label: 'Texto (costo: 50 de oro)',
        type:'radio',
        value:'texto'
      })
    }
    this.iniciar_juego();
  }


  mostrar_pista(tipo){
    if(this.oro >= this.costo_pista_txt ){
      if(tipo =="texto"){
        this.pista_txt = true;
        this.opciones_alert.inputs.length = 1
        this.restar_oro(this.costo_pista_txt);
      }else if(tipo=="casilla"){
        let campos_llenar = [];
        for (var i = 0; i < this.letras_respuesta.length; i++) {
            let escogido = this.letras_respuesta[i];
            if(escogido.letra != escogido.rspta && escogido.espacio != true ){
              campos_llenar.push(i);
            }
          }
        if(campos_llenar.length > 0 ){
          let max = campos_llenar.length -1 ;
          let index_random = (Math.random() * (max - 0) + 0).toFixed(0);
          let escogido = campos_llenar[index_random];
          this.poner_pista(escogido);
          this.indices_a_llenar.splice(+index_random,1)
        }
        this.restar_oro(this.costo_pista_casilla);
      }
    }
  }

  poner_pista(indice){
    let letra_rsp = this.letras_respuesta[indice];
    let indice_a_mover ;
    for (var i = 0; i < this.teclado_resp.length; i++) {
      if( letra_rsp.rspta == this.teclado_resp[i].letra && this.teclado_resp[i].estado == "activo" ){
        indice_a_mover = i;
      }
    }
    let letra_asignada = this.teclado_resp[indice_a_mover].letra;
    if(this.letras_respuesta[indice].estado == "activo"){
      this.regresar_letra(indice);
    }
    this.letras_respuesta[indice].letra = letra_asignada;
    this.letras_respuesta[indice].estado = "activo-pista";
    this.letras_respuesta[indice].blockear = true;
    this.teclado_resp[indice_a_mover].estado = "inactivo";
    this.indice_llenar = null;
    this.validar_respuesta();
  }

  private crear_arreglo_respuesta(){
    this.letras_respuesta.length = 0;
    for (var i = 0; i < this.respuesta.length; i++) {
      let letra = this.respuesta.substr(i,1);
      let modelo = {letra: "", rspta : letra , estado : "inactivo", espacio : false, origen: ""};
      if(letra == " "){
        modelo.espacio = true;
      }
      this.letras_respuesta.push(modelo);
    }
  }

  crear_teclado(){
    this.rellenar_teclado();
    this.indices_a_llenar.length = 0;
    this.indices_a_llenar = this.obtener_indices_a_remplazar();
    for (var i = 0; i < this.indices_a_llenar.length; i++) {
      let letra = this.respuesta.substr(i,1);
      if(letra != " "){
        let indice = this.indices_a_llenar[i];
        this.teclado_resp[indice].letra = letra;
        this.teclado_resp[indice].origen = i;
      }
    }
  }
  public validar_respuesta(){
    let intento = "";
    for (var i = 0; i < this.letras_respuesta.length; i++) {
      intento += this.letras_respuesta[i].letra;
    }
    if(intento == this.respuesta.replace( /\s/g, "")){
      this.mostrar_transicion();
    }else{
      //#code
    }
  }
  obtener_indices_a_remplazar(){
    let indices_array = [];
    for (var i = 0; i < this.respuesta.length; i++) {
      let max = this.teclado_resp.length - 1 ;
      let index_random_teclado = (Math.random() * (max - 0) + 0).toFixed(0);
      let validar = this.verificar_repeticion(indices_array,index_random_teclado);
      if( validar == false ){
        indices_array.push(index_random_teclado);
      }else{
        let array = this.obtener_indices_a_remplazar();
        if(array != null){
          return array;
        }
      }
    }
    if(indices_array.length == this.respuesta.length){
      return indices_array;
    }else{
      return null;
    }
  }

  verificar_repeticion(array,valor){
    for (var i = 0; i < array.length; i++) {
        if(array[i] == valor){
          return true;
        }
   }
   return false;
  }
  
  rellenar_teclado(){
    this.teclado_resp.length = 0;
    for (var i = 1; i <= 18; i++) {
      let max = this.banco_letras.length - 1 ;
      let index_random = (Math.random() * (max - 0) + 0).toFixed(0);
      let letra = this.banco_letras[index_random];
      let modelo = {letra: letra, estado : "activo"};
      this.teclado_resp.push(modelo);
    }
  }
  regresar_letra(indice){
    if(this.letras_respuesta[indice].letra.length > 0 ){
      if(this.letras_respuesta[indice].blockear != true && this.letras_respuesta[indice].espacio != true ){
        let cont = 0;
        for (var a = indice ; a < this.letras_respuesta.length; a++) {
          if(this.letras_respuesta[a].estado == "inactivo"){
            cont++;
          }
        }
        let spacios_blank = (this.letras_respuesta.length -1) - indice ;
        if(spacios_blank != cont){
          this.indice_llenar = indice;
        }
        this.letras_respuesta[indice].letra = "";
        this.letras_respuesta[indice].estado = "inactivo";
        let indice_teclado = +this.letras_respuesta[indice].origen;
        this.teclado_resp[indice_teclado].estado = "activo";
        this.letras_respuesta[indice].origen = "";
      }
    }
  }

  buscar_indice_siguiente(){
    for (var i = 0; i < this.letras_respuesta.length; i++) {
      if(this.letras_respuesta[i].letra.length == 0 && this.letras_respuesta[i].espacio != true ){
        return i;
      }
     }
     return -1;
  }
  
  asignar_letra(indice){
    let i = this.buscar_indice_siguiente();
    if( i != -1 && this.teclado_resp[indice].estado == "activo"){
      if(this.indice_llenar !=null){
        i = this.indice_llenar;
      }
      this.letras_respuesta[i].letra = this.teclado_resp[indice].letra;
      this.letras_respuesta[i].origen = indice;
      this.teclado_resp[indice].estado = "inactivo";
      this.letras_respuesta[i].estado = "activo";
      this.indice_llenar = null;
    }
    this.validar_respuesta();
  }

  mostrar_opciones_pista() {
    const alert = this.alertCtrl.create(this.opciones_alert);
    alert.present();
  }
}
