import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { preguntas } from './preguntas';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AdMobPro } from '@ionic-native/admob-pro';

@Injectable()

export class Servidor {
    public preguntas = preguntas;
    public idBanner = "ca-app-pub-3045632890392599/6309018718";
    public idInterstitial = "ca-app-pub-3045632890392599/4060714618";
    
    constructor( private storage : Storage,
                 public ga: GoogleAnalytics,
                 public admob: AdMobPro ) {
                    //this.storage.remove('data')
    }

    obtener_data_user(){
        return this.storage.get("data")
        .then(data=> data)
        .catch(this.OcurrioUnError)
    }

    public iniciar_interstitial(){
        this.admob.prepareInterstitial({
            adId : this.idInterstitial,
        })
        .then(()=>{
            this.admob.showInterstitial()
        })
        .catch((error)=>{
        })
    }

    public iniciar_banner(){
        this.admob.createBanner({
          adId : this.idBanner,
          autoShow : true,
          isTesting : false
        })
        .then((data)=>{
          this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER)
        })
        .catch((error)=>{

        })
    }

    guardar_data_user(data){
        this.storage.set("data",data)
    }
    private OcurrioUnError(error:any){
        return Promise.reject( error.message || error );
    }
}