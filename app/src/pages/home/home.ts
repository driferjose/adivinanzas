import { Component } from '@angular/core';
import { NavController, Events  } from 'ionic-angular';
import { Servidor } from '../../servicios/servidor.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers : [Servidor]
})

export class HomePage {
  private nivel_max = 1;
  private nivel_escogido = 1;
  private oro_actual = 500;

  constructor( public navCtrl: NavController,
               private servidor : Servidor,
               public events: Events  ) {
    this.iniciar_suscripciones();
  }
  
  ionViewDidLoad() {
    setTimeout(() => {
      this.iniciar_analitics()
      this.iniciar_banner();
      //this.servidor.iniciar_interstitial();
      //this.servidor.admob.showInterstitial()
    }, 500)
  }    

  private iniciar_suscripciones(){
    this.obtener_data_user();
    this.events.subscribe('juego_fin', (data) => {
      if(data.nivel_max > this.nivel_max){
        this.nivel_max = data.nivel_max;
      }
      this.nivel_escogido = data.nivel_escogido;
      this.oro_actual = data.oro_actual;
    });
    this.events.subscribe('nuevo_nivel', (data) => {
      this.nivel_escogido = data.nivel_escogido;
      this.guardar_data_user();
    });
    this.events.subscribe('canjeado', (data) => {
      if( data.oro > this.oro_actual){
        this.oro_actual = data.oro;
        this.guardar_data_user();
      }
    });
    this.servidor.admob.onAdDismiss()
    .subscribe(() => { 

    });
  }

  private iniciar_banner(){
    this.servidor.iniciar_banner();
  }

  private guardar_data_user(){
    let packete = {
      nivel_max : this.nivel_max,
      nivel_escogido : this.nivel_escogido,
      oro_actual : this.oro_actual
    }
    this.servidor.guardar_data_user(packete);
  }

  public ir_pagina(pag){
    let parametros = {
      datos_user : {
        nivel_max : this.nivel_max,
        nivel_escogido : this.nivel_escogido,
        oro_actual : this.oro_actual
      }
    }
    this.servidor.ga.trackEvent("Menu principal","ir a pagina",pag)
    
    this.navCtrl.push(pag,parametros);
  }
  
  public obtener_data_user(){
    this.servidor.obtener_data_user()
    .then(data=>{
      if(data != null){
        this.nivel_max = data.nivel_max;
        this.nivel_escogido = data.nivel_escogido;
        this.oro_actual = data.oro_actual
      }
    })
  }

  iniciar_analitics(){
     this.servidor.ga.startTrackerWithId('UA-90992958-2')
    .then(() => {
      this.servidor.ga.trackView('Menu principal');
    })
    .catch(e => {

    });
  }
}
