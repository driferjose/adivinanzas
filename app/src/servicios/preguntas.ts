export const preguntas={
  preguntas: [
    {
      "pregunta": "Cuanto más caliente, más fresco y crujiente",
      "respuesta": "PAN",
      "pista": "se come todos los dias"
    },
    {
      "pregunta": "Un platito de avellanas que de día se recogen y de noche se desparraman.",
      "respuesta": "estrellas",
      "pista": "Están en el espacio"
    },
    {
      "pregunta": "Pobrecito, pobrecito, todo el día sin parar y no sale de su sitio.",
      "respuesta": "reloj",
      "pista": "Sirve para ver la hora"
    },
    {
      "pregunta": " Tengo nombre de animal, cuando la rueda se pincha me tienes que utilizar. ",
      "respuesta": "gata",
      "pista": "Tengo bigote"
    },
    {
      "pregunta": " Todos me quieren para descansar y si ya te lo he dicho!! no pienses más. ",
      "respuesta": "silla",
      "pista": "Te puedes sentar en ella"
    },
    {
      "pregunta": " Te la digo y no me entiendes, te la repito y no me comprendes. ",
      "respuesta": "tela",
      "pista": "Sirve para hacer ropa"
    },
    {
      "pregunta": " Soy ave y soy llana, pero no tengo pico ni alas. ",
      "respuesta": "avellana",
      "pista": "Fruto del avellano"
    },
    {
      "pregunta": " Me gustaría ser tigre pero no tengo su altura, cuando escuches un \"miau\" lo adivinarás sin duda ",
      "respuesta": "gato",
      "pista": "Soy un felino"
    },
    {
      "pregunta": " Llevo dinero y no soy banquero, papel o metal, lo que sea me da igual. ",
      "respuesta": "cartera",
      "pista": "También guardo tarjetas"
    },
    {
      "pregunta": " Redondo soy como un pandero, quien me tome en verano que use sombrero. ",
      "respuesta": "sol",
      "pista": "Hago mucha luz"
    },
    {
      "pregunta": " Es pequeña como una pera, pero alumbra la casa entera. ",
      "respuesta": "bombilla",
      "pista": "Es eléctrica"
    },
    {
      "pregunta": " En rincones y entre ramas mis redes voy construyendo, para que moscas incautas, en ellas vayan cayendo. ",
      "respuesta": "araña",
      "pista": "Tengo muchas patas"
    },
    {
      "pregunta": " Y lo es Y lo es y no lo adivinarás aunque te dé un mes. ",
      "respuesta": "hilo",
      "pista": "Puedes tirar de él"
    },
    {
      "pregunta": " ¿Cuál es de los animales aquel que en su nombre tiene las cinco vocales? ",
      "respuesta": "murciélago",
      "pista": " Duerme colgado boca abajo"
    },
    {
      "pregunta": " Verde nace, verde se cría y verde sube los troncos arriba. ",
      "respuesta": "lagartija",
      "pista": " Es un reptil"
    },
    {
      "pregunta": "Si lo escribes como es, soy de la selva el rey. Si lo escribes al revés soy tu Papá Noel. ",
      "respuesta": "león",
      "pista": "Es el rey de la selva"
    },
    {
      "pregunta": " Murcia me da medio nombre,una letra has de cambiar,mas cuando llegues al lago,mi nombre podrás terminar. ",
      "respuesta": "murciélago",
      "pista": " Vive en la oscuridad"
    },
    {
      "pregunta": " Es la reina de los mares,su dentadura es muy buena,y por no ir nunca vacía,siempre dicen que va llena. ",
      "respuesta": "ballena",
      "pista": "Vive en el mar pero es un mamífero"
    },
    {
      "pregunta": " Verde como el campo, pero campo no es. Habla como el hombre, pero hombre no es. ",
      "respuesta": "loro",
      "pista": "Vuela"
    },
    {
      "pregunta": " Ven al campo por las noches si me quieres conocer,soy señor de grandes ojos cara seria y gran saber. ",
      "respuesta": "buho",
      "pista": " Son muy sigiloso"
    },
    {
      "pregunta": "Tengo tinta y tengo plumas y brazos tengo de más pero no puedo escribir,porque no aprendí jamás. ",
      "respuesta": "calamar",
      "pista": " Se come"
    },
    {
      "pregunta": " Yo no quiero que os canséis y por eso os recomiendo que esta adivinanza miréis, para quitarle un perro y su número obtendréis. ",
      "respuesta": "Seis",
      "pista": " La solución está en la adivinanza"
    },
    {
      "pregunta": "Dos abanicos que danzan todo el día sin parar; y cuando por fin te duermas quietecitos quedarán.",
      "respuesta": "pestañas",
      "pista": "Cuando se te cae una, debes pedir un deseo"
    },
    {
      "pregunta": " De millones de hijos que somos el primero nací aún así soy el menor de todos ¿cómo puede ser así? ",
      "respuesta": "uno",
      "pista": "Número de menor valor"
    },
    {
      "pregunta": " Soy más de uno sin llegar al tres, y llego a cuatro cuando dos me des. ",
      "respuesta": "dos",
      "pista": "Entre el 1 y el 3"
    },
    {
      "pregunta": "La virgen me dio tu nombre, el señor me dio tu amor, tu me diste un beso y yo te di mi.....",
      "respuesta": "Corazón",
      "pista": "Sientes los latidos dentro de ti"
    },
    {
      "pregunta": " Las estaciones del año y también los elementos y los puntos cardinales, ese número represento. ",
      "respuesta": "cuatro",
      "pista": "¿Cuántas estaciones hay?"
    },
    {
      "pregunta": " ¿Qué cosa será esa que cuando la miras del derecho y mirada del revés siempre un número es? ",
      "respuesta": "nueve",
      "pista": "Gira un número de posición"
    },
    {
      "pregunta": " ¿Cuáles son los números más pesimistas? ",
      "respuesta": "negativos.",
      "pista": "Los positivos no lo son"
    },
    {
      "pregunta": " Tengo forma de serpiente pero no la que más miente. ",
      "respuesta": "tres.",
      "pista": "Menor de 5"
    },
    {
      "pregunta": "Son 28 caballeros de espaldas negras y lisas; delante, todo agujeros, por dominar se dan prisa. ",
      "respuesta": "dominó",
      "pista": "EL  D_ MI_ _"
    },
    {
      "pregunta": " Hay gatos en un cajón, cada gato en un rincón, cada gato ve tres gatos ¿Sabes cuántos son? ",
      "respuesta": "Cuatro. ",
      "pista": "¿Cuántas esquinas tiene un cajón?"
    },
    {
      "pregunta": "Un circulito muy redondito que cuando le pegas brincas de susto. ",
      "respuesta": "tambor",
      "pista": "E_ TAM_ OR"
    },
    {
      "pregunta": " Una vieja arrugadita que de joven daba vino y ahora es una frutita. ",
      "respuesta": "pasa",
      "pista": " Es buena para la memoria"
    },
    {
      "pregunta": " Verde por fuera roja por dentro y con bailarinas en el centro. ",
      "respuesta": "sandía",
      "pista": " Las hay con pepitas"
    },
    {
      "pregunta": " Vengo de padres cantores aunque yo no soy cantor,traigo los hábitos blancos y amarillo el corazón. ",
      "respuesta": "huevo",
      "pista": " Se rompe para hacer tortillas"
    },
    {
      "pregunta": "Yo soy el diminutivo de una fruta muy hermosa,tengo virtud provechosa,en el campo siempre vivo y mi cabeza es vistosa. ",
      "respuesta": "manzanilla",
      "pista": " Se utiliza para las infusiones"
    },
    {
      "pregunta": " Zorra le dicen, ya ves,aunque siempre del revés,se lo come el japonés y plato muy rico es.¿Qué es? ",
      "respuesta": "arroz",
      "pista": " Cuál es el elemento principal de la paella?"
    },
    {
      "pregunta": " Campanita, campanera,blanca por dentro, verde por fuera,si no lo adivinas,piensa y espera. ",
      "respuesta": "pera",
      "pista": " Es una fruta"
    },
    {
      "pregunta": " Si quieres las tomas y si no las dejas,aunque suelen decir que son comida de viejas. ",
      "respuesta": "lentejas",
      "pista": " Es un plato de comida"
    },
    {
      "pregunta": " Una madre con cien hijas y a todas pone camisas. ",
      "respuesta": "granada",
      "pista": " Es una fruta"
    },
    {
      "pregunta": " Ave tengo yo por nombre,llana es mi condición.Él que no acierte mi nombre,es porque no presta atención. ",
      "respuesta": "avellana",
      "pista": " Viene del avellano"
    },
    {
      "pregunta": " Blancos y larguiruchos,nos fríen en la verbena,dorados y calentitos,nos comen el nene y la nena. ",
      "respuesta": "churros",
      "pista": " Con chocolate"
    },
    {
      "pregunta": " Cinco hermanos muy unidos que no se pueden mirar, cuando riñen aunque quieras no los puedes separar. ",
      "respuesta": "dedos",
      "pista": " Los tenemos en las manos"
    },
    {
      "pregunta": "Dicen que tiene y no tiene, mucho pincha, poco retiene.",
      "respuesta": "tenedor",
      "pista": "TENED_ R"
    },
    {
      "pregunta": " Si me nombras desaparezco, ¿quien soy? ",
      "respuesta": "silencio",
      "pista": "No se escuchan sonidos"
    },
    {
      "pregunta": " Iba una vaca de lado, luego resultó pescado. ",
      "respuesta": "bacalao",
      "pista": "Pez de tamaño pequeño"
    },
    {
      "pregunta": " Desde el lunes hasta el viernes, soy la última en llegar, el sábado soy la primera y el domingo a descansar ",
      "respuesta": "S",
      "pista": "Está en el abecedario"
    },
    {
      "pregunta": " Tengo nombre de mujer, crezco en el fondo del mar, en la arena de la playa tú me podrás encontrar. ",
      "respuesta": "concha",
      "pista": " Los moluscos tienen"
    },
    {
      "pregunta": " Lleva años en el mar y aún no sabe nadar. ",
      "respuesta": "arena",
      "pista": " En la playa hay mucha"
    },
    {
      "pregunta": " No son flores, pero tienen plantas y olores. ",
      "respuesta": "pies",
      "pista": " Son el final de las piernas"
    },
    {
      "pregunta": " Estoy en la guerra y huyo del cañón. Estoy en el altar y no estoy en la iglesia. Estoy en la flor y no en la planta. ",
      "respuesta": "R",
      "pista": " Está en el abecedario"
    },
    {
      "pregunta": " Dos hermanas diligentes que caminan al compás, con el pico por delante y los ojos por detrás. ",
      "respuesta": "tijeras",
      "pista": " Sirven para cortar"
    },
    {
      "pregunta": "Cabeza de hierro,cuerpo de madera,si te piso un dedo,! menudo grito pegas !",
      "respuesta": "martillo",
      "pista": " Sirve para golpear"
    },
    {
      "pregunta": " Me llegan las cartas y no sé leer y, aunque me las trago,no mancho el papel. ",
      "respuesta": "buzón",
      "pista": " Donde se introducen las cartas"
    },
    {
      "pregunta": " Siempre me arrinconan sin acordarse de mí,pero pronto que me quieren cuando tienen que subir. ",
      "respuesta": "escalera",
      "pista": " Tiene peldaños"
    },
    {
      "pregunta": " Tiene agujas y no cose,no se mueve, pero anda,si le das cuerda funciona y el paso del tiempo señala. ",
      "respuesta": "reloj",
      "pista": " Los hay de pulsera"
    },
    {
      "pregunta": " Es grande como un ratón,pero guarda la casa como un león. ",
      "respuesta": "llave",
      "pista": " Abre cerraduras"
    },
    {
      "pregunta": " En tus manos limpio, en tus ventanas sucio,si sucio, me ponen limpio, si limpio, me ponen sucio. ",
      "respuesta": "pañuelo",
      "pista": " Los hay de papel"
    },
    {
      "pregunta": " Cien amigas tengo,cien sobre una tabla,pero si no las tocas,no te dirán nada. ",
      "respuesta": "piano",
      "pista": " Instrumento musical"
    },
    {
      "pregunta": " Soy bonito por delante, algo feo por detrás;me transformo a cada instante,ya que imito a los demás. ",
      "respuesta": "espejo",
      "pista": " Ves tu reflejo en él"
    },
    {
      "pregunta": " En las manos de las damas a veces estoy metido,unas veces estirado y otras veces encogido. ",
      "respuesta": "abanico",
      "pista": " Con él de avientas"
    },
    {
      "pregunta": " Aparece por delante,por los lados, por la espalda,te descuidas un instante y te levanta la falda. ",
      "respuesta": "viento",
      "pista": " Es aire en movimiento"
    },
    {
      "pregunta": " De la mar salió mi nombre y tan desgraciada nací que huyendo de mi desgracia contra una garita di. ",
      "respuesta": "margarita",
      "pista": " Flor"
    },
    {
      "pregunta": " Aparece por delante, por los lados, por la espalda, te descuidas un instante y te levanta la falda. ",
      "respuesta": "viento",
      "pista": " Aire en movimiento"
    },
    {
      "pregunta": " Nace en el monte, muere en el mar, y nunca regresa a su lugar ",
      "respuesta": "río",
      "pista": " Corriente de agua"
    },
    {
      "pregunta": " ¿Qué será lo que es? Que mientras más grande, menos se ve. ",
      "respuesta": "oscuridad",
      "pista": " No es luminosa"
    },
    {
      "pregunta": " Salimos cuando anochece, nos vamos si canta el gallo, y hay quien dice que nos ve cuando le pisan un callo. ",
      "respuesta": "estrellas",
      "pista": " Hay muchas"
    },
    {
      "pregunta": " Que es una cosa extraña a la que todos tememos, que cuanto más grande se hace, menos y menos la vemos. ",
      "respuesta": "oscuridad",
      "pista": " Sin luminosidad"
    },
    {
      "pregunta": " Somos más de mil hermanas que bajo el mismo techo vivimos, por la noche estamos de fiesta y por el día dormimos. ",
      "respuesta": "estrellas",
      "pista": " Son luminosas"
    },
    {
      "pregunta": " Está en el edificio, también en la maceta, la llevas en el pié, la coges en la huerta. ",
      "respuesta": "planta",
      "pista": " La plan**"
    },
    {
      "pregunta": " ¿Quién es algo y nada a la vez? ",
      "respuesta": "pez",
      "pista": " Nada en el mar o en el río"
    },
    {
      "pregunta": " Con mi cara roja, mi ojo negro y mi vestido verde a todo el campo alegro. ",
      "respuesta": "amapola",
      "pista": " Es una flor"
    },
    {
      "pregunta": " Es cómo una paloma blanca y negra, pero vuela sin alas y habla sin lengua ",
      "respuesta": "carta",
      "pista": " A veces va dentro de un sobre"
    },
    {
      "pregunta": " Alto alto como un pino, pesa menos que un comino. ",
      "respuesta": "humo",
      "pista": " Se produce al quemar algo"
    },
    {
      "pregunta": " Una caja pequeñita, blanquita como la cal, todo la saben abrir, nadie la sabe cerrar. ",
      "respuesta": "huevo",
      "pista": " Con ellos se hacen las tortillas"
    },
    {
      "pregunta": " En el mar no me mojo, en las brasas no me quemo, en el aire no me caigo y me tienes en los labios. ",
      "respuesta": "A",
      "pista": " La primera del abecedario"
    },
    {
      "pregunta": " Todos pasan por mí, yo nunca paso por nadie. Todos preguntan por mí, yo nunca pregunto por nadie. ",
      "respuesta": "calle",
      "pista": " Sales de casa y vas a la...?"
    },
    {
      "pregunta": " Tul y no es tela, pan, pero no de mesa. ",
      "respuesta": "tulipán",
      "pista": " Es una planta"
    },
    {
      "pregunta": " Conteste, don Serafín, en prosa, en verso, o en inglés, qué es lo que tiene principio, pero no tiene fin. ",
      "respuesta": "números",
      "pista": " Cuentas con ellos"
    },
    {
      "pregunta": " Soy un anciano arrugadito que si me echan al agua me pongo gordito. ",
      "respuesta": "garbanzo",
      "pista": " Una legumbre"
    },
    {
      "pregunta": " Dentro de una vaina estoy y ni espada ni sable soy. ",
      "respuesta": "guisante",
      "pista": " De color verde"
    },
    {
      "pregunta": " Si quieres las tomas y si no las dejas, aunque dicen que son comida de viejas. ",
      "respuesta": "lentejas",
      "pista": " Redondas y marrones"
    },
    {
      "pregunta": " En la tierra te sembraron, los pájaros te desearon, cuando estuviste dorado los hombres te segaron. ",
      "respuesta": "Trigo",
      "pista": " Cereal"
    },
    {
      "pregunta": " Me llegan las cartas y no sé leer y, aunque me las trago, no mancho ni un poco el papel. ",
      "respuesta": "buzón",
      "pista": " Está en todas las casas"
    },
    {
      "pregunta": " No es león y tiene garra, no es pato y tiene pata. ",
      "respuesta": "garrapata",
      "pista": " Insecto"
    },
    {
      "pregunta": " A ti acudo de pequeño en ti maduro, trabajo y estudio para el futuro. ",
      "respuesta": "colegio",
      "pista": " Donde haces muchos amigos"
    },
    {
      "pregunta": " Tengo muchos pares, y te los puedes probar si tú te los llevas tendrás que pagar. ",
      "respuesta": "zapatería",
      "pista": " Una tienda"
    },
    {
      "pregunta": " Tiene ojos y no es para nada bajo, ríos y arroyuelos pasan por debajo. ",
      "respuesta": "puente",
      "pista": " Pasan coches o personas"
    },
    {
      "pregunta": " Mido telas y estaturas, pero, a veces, en ciudades, sin humos y sin candelas, llevo personas en cantidades. ",
      "respuesta": "metro",
      "pista": " Por debajo de tierra"
    },
    {
      "pregunta": " El que la tenga que la atienda y si no lo mejor es que la venda. ",
      "respuesta": "tienda",
      "pista": " Compras cosas en ella"
    },
    {
      "pregunta": " Verde soy, y verde siempre seré, no me toques que te picaré. ",
      "respuesta": "ortiga",
      "pista": " Una planta"
    },
    {
      "pregunta": " ¿Qué es, qué no es? Está en el jardín, pero también en tus pies. ",
      "respuesta": "planta",
      "pista": " Es verde"
    },
    {
      "pregunta": " De tus tíos es hermana, es hija de tus abuelos y quién más a ti te ama. ",
      "respuesta": "madre",
      "pista": " La quieres mucho"
    },
    {
      "pregunta": " Durante el verano escondido,y en el frío invierno encendido. ",
      "respuesta": "brasero",
      "pista": " Te da calor"
    },
    {
      "pregunta": " ¿Quién pensaréis que yo soy, que cuanto más y más lavo mucho más sucia me voy? ",
      "respuesta": "agua",
      "pista": " Es transparente"
    },
    {
      "pregunta": " Estoy dentro de él y no puedo entrar en él. ",
      "respuesta": "espejo",
      "pista": " Te miras en él"
    },
    {
      "pregunta": " Cuanto más se moja más te seca. ¿Qué es?",
      "respuesta": "toalla",
      "pista": " Es suave"
    },
    {
      "pregunta": " Aunque al dormir me consultas nunca te voy a contestar. ",
      "respuesta": "lmohada",
      "pista": " Es cómoda"
    },
    {
      "pregunta": "I am not alive but I can grow. I do not have lungs but I need air to survive. What am I?",
      "respuesta": "Fire",
      "pista": " It burns you"
    }
  ]
}

