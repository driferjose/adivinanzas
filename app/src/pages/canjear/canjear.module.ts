import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanjearPage } from './canjear';

@NgModule({
  declarations: [
    CanjearPage,
  ],
  imports: [
    IonicPageModule.forChild(CanjearPage),
  ],
})
export class CanjearPageModule {}
