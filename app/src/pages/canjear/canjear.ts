import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Servidor } from '../../servicios/servidor.service';

@IonicPage()
@Component({
  selector: 'page-canjear',
  templateUrl: 'canjear.html',
  providers : [Servidor]
})

export class CanjearPage {
  public oro_actual = 0;
  private oro_ganado = 50;
  private interval;
  private txt_share = "Adivinanzas, ¡pon a prueba tu conocimiento! http://play.google.com/store/apps/details?id=celerixcode.adivinanzas";
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private socialSharing: SocialSharing,
              private servidor : Servidor,
              public events: Events  ) {
    this.recibir_parametros()
  }

  ionViewDidLoad() {
    this.servidor.ga.trackView('Pagina de canje');
  }

  ionViewDidLeave(){
    this.events.publish("canjeado",{oro :this.oro_actual})
    this.servidor.iniciar_interstitial();
  }
  
  private recibir_parametros(){
    let parametros = this.navParams.get("datos_user");
    this.oro_actual = parametros.oro_actual;
  }

  iniciar_compartir(tipo){
   if(tipo == "fb"){
      this.socialSharing
      .shareViaFacebook(this.txt_share).then((data) => {
          if(data == "OK"){
            this.servidor.ga.trackEvent("Canjear","Compartir","facebook")
            this.animar_oro();
          }
      }).catch(() => {
        // Sharing via email is not possible
      });
   }else if(tipo == "tw"){
      this.socialSharing
      .shareViaTwitter(this.txt_share).then((data) => {
        if(data == "OK"){
          this.servidor.ga.trackEvent("Canjear","Compartir","Twitter")
          this.animar_oro();
        }
      }).catch(() => {
        // Sharing via email is not possible
      });
   }else if (tipo == "ws"){
      this.socialSharing
      .shareViaWhatsApp(this.txt_share).then((data) => {
        if(data == "OK"){
          this.servidor.ga.trackEvent("Canjear","Compartir","Whatsapp")
          this.animar_oro();
        }
      }).catch(() => {
        // Sharing via email is not possible
      });
   }
  }

  animar_oro(){
    this.iniciar_interval();
  }

  iniciar_interval(){
    let este = this;
    let total = this.oro_actual + this.oro_ganado;
    let pasos = this.oro_ganado * 0.1

    this.interval = setInterval(()=> {
      if(este.oro_actual < total){
        este.oro_actual += pasos;
      }else{
        clearInterval(este.interval); 
      }
    }, 50)
  }
}
