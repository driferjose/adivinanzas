import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransicionPage } from './transicion';

@NgModule({
  declarations: [
    TransicionPage,
  ],
  imports: [
    IonicPageModule.forChild(TransicionPage),
  ],
})
export class TransicionPageModule {}
